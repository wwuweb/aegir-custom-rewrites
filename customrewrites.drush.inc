<?php

/**
 * @file
 * Custom Apache mod_rewrite rules for Aegir managed Drupal sites.
 */

/**
 * Implements hook_provision_apache_vhost_config().
 */
function customrewrites_provision_apache_vhost_config($uri, $data) {
  if (preg_match('@^admissions(prod|)\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = '<IfModule mod_rewrite.c>';
    $rval[] = '  RewriteEngine on';
    $rval[] = '  RewriteRule ^/print/(.*) https://admissions.wwu.edu/files/print/$1 [redirect=permanent,last]';
    $rval[] = '</IfModule>';

    $rval[] = '';

    return $rval;
  }

  if (preg_match('@^cbe(prod|)\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = 'ServerAlias cebr.wwu.edu';

    $rval[] = '<IfModule mod_rewrite.c>';
    $rval[] = '  RewriteEngine on';
    $rval[] = '  RewriteCond %{HTTP_HOST} ^cebr.wwu.edu$';
    $rval[] = '  RewriteRule ^/.* https://cbe.wwu.edu/cebr/center-economic-and-business-research [redirect=permanent,last]';
    $rval[] = '</IfModule>';

    $rval[] = '';

    return $rval;
  }

  if (preg_match('@^chss(prod|)\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = '<IfModule mod_alias.c>';
    $rval[] = '  Redirect 301 /csd/western-speech-and-hearing-clinics /csd/speech-and-language-services';
    $rval[] = '</IfModule>';

    $rval[] = '';
  }

  if (preg_match('@^cse(prod|)\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = 'ServerAlias chem.wwu.edu';

    $rval[] = '<IfModule mod_rewrite.c>';
    $rval[] = '  RewriteEngine on';
    $rval[] = '  RewriteCond %{HTTP_HOST} ^chem.wwu.edu$';
    $rval[] = '  RewriteRule ^/.* http://cse.wwu.edu/chemistry [redirect=permanent,last]';
    $rval[] = '</IfModule>';

    $rval[] = '';

    $rval[] = 'ServerAlias physics.wwu.edu';

    $rval[] = '<IfModule mod_rewrite.c>';
    $rval[] = '  RewriteEngine on';
    $rval[] = '  RewriteCond %{HTTP_HOST} ^physics.wwu.edu$';
    $rval[] = '  RewriteRule ^/manuals http://physics-astronomy-manuals.wwu.edu/ [redirect=permanent,last]';
    $rval[] = '  RewriteCond %{HTTP_HOST} ^physics.wwu.edu$';
    $rval[] = '  RewriteRule ^/.* http://cse.wwu.edu/physics [redirect=permanent,last]';
    $rval[] = '</IfModule>';

    $rval[] = '';

    return $rval;
  }

  if (preg_match('@^library(prod|)\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = '<IfModule mod_alias.c>';
    $rval[] = '  RedirectMatch "^/onesearch(.*)" "http://libweb1.library.wwu.edu/onesearch$1"';
    $rval[] = '  RedirectMatch "^/goto/?(.*)"    "http://libweb1.library.wwu.edu/goto/$1"';
    $rval[] = '</IfModule>';

    $rval[] = '';

    return $rval;
  }

  if (preg_match('@^policy(dev|test|prod|)?(\dx\d+)?\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = '<IfModule mod_rewrite.c>';
    $rval[] = '  RewriteEngine on';
    $rval[] = '  RewriteRule ^([^\s]+)\ (.*\.pdf)$ $1-$2 [discardpath,next,env=NOSPACE:1]';
    $rval[] = '  RewriteCond %{ENV:NOSPACE} =1';
    $rval[] = '  RewriteRule ^([^\s]+)$ $1 [redirect=permanent,last]';
    $rval[] = '</IfModule>';

    $rval[] = '';

    return $rval;
  }

  if (preg_match('@^president(prod|)\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = 'ServerAlias presidentdesignate.wwu.edu';

    $rval[] = '<IfModule mod_rewrite.c>';
    $rval[] = '  RewriteEngine on';
    $rval[] = '  RewriteCond %{HTTP_HOST} ^presidentdesignate.wwu.edu$';
    $rval[] = '  RewriteRule ^/.* https://president.wwu.edu [redirect=permanent,last]';
    $rval[] = '</IfModule>';

    $rval[] = '';

    return $rval;
  }

  if (preg_match('@^wce(prod|)\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $rval[] = '';

    $rval[] = '<IfModule mod_alias.c>';
    $rval[] = ' Redirect 301 /nwchgee/northwest-center-holocaust-genocide-and-ethnocide-education https://wp.wwu.edu/raywolpowinstitute';
    $rval[] = '</IfModule>';

    return $rval;
  }
}
